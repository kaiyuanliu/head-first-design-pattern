import fly.FlyRocketPowered;

/**
 * Created by kaiyuan on 5/5/16.
 */
public class MainApp {
    public static void main(String[] args) {
        Duck duck = new MallardDuck();

        duck.performQuack();

        duck.performFly();

        duck.setFlyBehavior(new FlyRocketPowered());

        duck.performFly();
    }
}
