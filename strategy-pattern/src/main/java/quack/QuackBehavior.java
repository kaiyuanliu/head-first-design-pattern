package quack;

/**
 * Created by kaiyuan on 5/5/16.
 */
public interface QuackBehavior {
    void quack();
}
