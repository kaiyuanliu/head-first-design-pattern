package quack;

/**
 * Created by kaiyuan on 5/5/16.
 */
public class Squeak implements QuackBehavior {
    public void quack() {
        System.out.println("Squeak Squeak Squeak");
    }
}
