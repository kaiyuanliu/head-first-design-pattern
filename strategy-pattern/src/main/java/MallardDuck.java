import fly.FlyWithWings;
import quack.Quack;

/**
 * Created by kaiyuan on 5/5/16.
 */
public class MallardDuck extends Duck {
    public MallardDuck() {
        flyBehavior = new FlyWithWings();
        quackBehavior = new Quack();
    }

    public void display() {
        System.out.println("I'm a real Mallard duck");
    }
}
