package fly;

/**
 * Created by kaiyuan on 5/5/16.
 */
public class FlyWithWings implements FlyBehavior {
    public void fly() {
        System.out.println("fly with wings");
    }
}
