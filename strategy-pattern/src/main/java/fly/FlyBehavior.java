package fly;

/**
 * Created by kaiyuan on 5/5/16.
 */
public interface FlyBehavior {
    void fly();
}
