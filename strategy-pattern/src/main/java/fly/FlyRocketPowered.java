package fly;

/**
 * Created by kaiyuan on 5/5/16.
 */
public class FlyRocketPowered implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("Im flying with a rocket");
    }
}
