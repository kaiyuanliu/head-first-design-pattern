package fly;

/**
 * Created by kaiyuan on 5/5/16.
 */
public class FlyNoWay implements FlyBehavior {
    public void fly() {
        System.out.println("fly no way");
    }
}
