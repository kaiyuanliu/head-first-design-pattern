import fly.FlyBehavior;
import quack.QuackBehavior;

/**
 * Created by kaiyuan on 5/5/16.
 */
public abstract class Duck {
    protected FlyBehavior flyBehavior;

    protected QuackBehavior quackBehavior;

    public void setFlyBehavior(FlyBehavior flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    public void setQuackBehavior(QuackBehavior quackBehavior) {
        this.quackBehavior = quackBehavior;
    }

    protected void performQuack() {
        quackBehavior.quack();
    }

    protected void performFly() {
        flyBehavior.fly();
    }

    protected abstract void display();

    protected void swim() {
        System.out.println("All ducks float, even decoys");
    }
}
