/**
 * Created by kaiyuan on 5/8/16.
 */
public interface DisplayElement {
    void display();
}
