import java.util.Observable;
import java.util.Observer;

/**
 * Created by kaiyuan on 5/8/16.
 */
public class CurrentConditionDisplay implements Observer, DisplayElement {
    private float temprature;
    private float humidity;
    Observable observable;

    public CurrentConditionDisplay(Observable observable) {
        this.observable = observable;
        this.observable.addObserver(this);
    }

    public void display() {
        System.out.println("Current conditions: " + temprature +
                "F degree and " + humidity + "% humidity");
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData weatherData = (WeatherData) o;
            this.temprature = weatherData.getTemperature();
            this.humidity = weatherData.getHumidity();
            display();
        }
    }
}
