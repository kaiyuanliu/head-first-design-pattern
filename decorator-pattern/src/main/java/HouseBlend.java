/**
 * Created by kaiyuan on 5/10/16.
 */
public class HouseBlend extends Beverage {

    public HouseBlend() {
        description = "House Blend Description";
    }

    @Override
    public double cost() {
        return 0.55;
    }
}
