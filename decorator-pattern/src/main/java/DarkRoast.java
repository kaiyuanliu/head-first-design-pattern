/**
 * Created by kaiyuan on 5/10/16.
 */
public class DarkRoast extends Beverage {

    public DarkRoast() {
        description = "Dark Roast Description";
    }

    @Override
    public double cost() {
        return 0.39;
    }
}
