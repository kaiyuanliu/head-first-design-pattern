/**
 * Created by kaiyuan on 5/10/16.
 */
public class Decaf extends Beverage {

    public Decaf() {
        description = "Decaf Description";
    }

    @Override
    public double cost() {
        return 0.45;
    }
}
