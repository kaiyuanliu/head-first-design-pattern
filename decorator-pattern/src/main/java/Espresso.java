/**
 * Created by kaiyuan on 5/10/16.
 */
public class Espresso extends Beverage {

    public Espresso() {
        description = "Espresso Description";
    }

    @Override
    public double cost() {
        return 0.99;
    }
}
