/**
 * Created by kaiyuan on 5/10/16.
 */
public class StarbuzzCoffee {
    public static void main(String[] args) {
        Beverage beverage = new DarkRoast();
        System.out.println("beverage: " + beverage.getDescription() + " | $" + beverage.cost());

        Beverage beverage1 = new Decaf();
        beverage1 = new Mocha(beverage1);
        beverage1 = new Mocha(beverage1);
        beverage1 = new Whip(beverage1);
        System.out.println("beverage: " + beverage1.getDescription() + " | $" + beverage1.cost());

        Beverage beverage2 = new HouseBlend();
        beverage2 = new Soy(beverage2);
        beverage2 = new Mocha(beverage2);
        beverage2 = new Whip(beverage2);
        System.out.println("beverage: " + beverage2.getDescription() + " | $" + beverage2.cost());

        Beverage beverage3 = new Espresso();
        System.out.println("beverage: " + beverage3.getDescription() + " | $" + beverage3.cost());
    }
}
