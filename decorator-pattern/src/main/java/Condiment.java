/**
 * Created by kaiyuan on 5/11/16.
 */
public abstract class Condiment extends Beverage {
    public abstract String getDescription();
}
